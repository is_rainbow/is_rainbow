# 高等数学复习

1. [高等数学复习](#高等数学复习)
   1. [高中遗留物](#高中遗留物)
   1. [函数](#函数)
      1. [一元函数](#一元函数)
      1. [三角函数](#三角函数)
      1. [计算法则](#计算法则)
      1. [定义域](#定义域)
      1. [函数单调性](#函数单调性)
      1. [函数凹凸性](#函数凹凸性)
      1. [判断函数间断点](#判断函数间断点)
   1. [极限](#极限)
      1. [极限类型](#极限类型)
      1. [极限做题时的性质](#极限做题时的性质)
      1. [极限计算法则](#极限计算法则)
      1. [近似值](#近似值)
         1. [利用无穷小的性质求函数的极限](#利用无穷小的性质求函数的极限)
      1. [重要极限](#重要极限)
      1. [无穷大小量](#无穷大小量)
         1. [等价无穷大小](#等价无穷大小)
      1. [常用解法](#常用解法)
         1. [抓大头](#抓大头)
      1. [常见的等价变换](#常见的等价变换)
      1. [泰勒展开](#泰勒展开)
         1. [其他型月世界的泰勒展开](#其他型月世界的泰勒展开)
      1. [极限的图形学](#极限的图形学)
      1. [其他型月世界的极限题](#其他型月世界的极限题)
   1. [导数](#导数)
      1. [导数运算法则](#导数运算法则)
      1. [导数公式](#导数公式)
      1. [隐函数求导](#隐函数求导)
         1. [隐函数求导例题](#隐函数求导例题)
      1. [可导导致的连续](#可导导致的连续)
      1. [分离变量](#分离变量)
   1. [偏导数](#偏导数)
   1. [积分](#积分)
      1. [分部积分法](#分部积分法)
      1. [变换积分次序](#变换积分次序)
      1. [不定积分](#不定积分)
      1. [不定积分公式](#不定积分公式)
      1. [经常考的傻逼积分变形](#经常考的傻逼积分变形)
      1. [其他型月世界常见积分](#其他型月世界常见积分)
      1. [定积分](#定积分)
      1. [定积分例题](#定积分例题)
      1. [二重积分](#二重积分)
      1. [变上限积分求导](#变上限积分求导)
      1. [对称限积分](#对称限积分)
      1. [圆相关的积分](#圆相关的积分)
   1. [微分](#微分)
      1. [微分运算法则](#微分运算法则)
      1. [微分方程](#微分方程)
      1. [简易微分方程的求解方法](#简易微分方程的求解方法)
         1. [一阶线性微分](#一阶线性微分)
         1. [一阶线性常微分方程](#一阶线性常微分方程)
         1. [二阶常系数齐次常微分方程x](#二阶常系数齐次常微分方程x)
      1. [其他型月世界得求微分通解](#其他型月世界得求微分通解)
      1. [全微分](#全微分)
      1. [微分在近似计算种的应用](#微分在近似计算种的应用)
   1. [级数](#级数)
      1. [级数的敛散](#级数的敛散)
         1. [P级数](#p级数)
         1. [交错P级数](#交错p级数)
         1. [调和级数](#调和级数)
         1. [和函数](#和函数)
      1. [其他型月世界的级数例题](#其他型月世界的级数例题)
   1. [各类理论](#各类理论)
      1. [中值定理/均值定理](#中值定理均值定理)
      1. [洛必达](#洛必达)
      1. [条件](#条件)
      1. [牛顿-莱布尼兹 公式](#牛顿-莱布尼兹-公式)
      1. [微分中值定理](#微分中值定理)
         1. [罗尔定理](#罗尔定理)
         1. [拉格朗日中值定理](#拉格朗日中值定理)
         1. [柯西定理](#柯西定理)
      1. [间断点](#间断点)
         1. [第一类间断点](#第一类间断点)
         1. [可去间断点](#可去间断点)
         1. [跳跃间断点](#跳跃间断点)
   1. [图形学](#图形学)
      1. [向量](#向量)
      1. [模长](#模长)
      1. [方向余弦](#方向余弦)
      1. [方向角](#方向角)
      1. [法向量](#法向量)
         1. [法向量定义](#法向量定义)
      1. [方向向量](#方向向量)
         1. [方向向量定义](#方向向量定义)
      1. [计算与公式](#计算与公式)
      1. [其他型月世界常见的图形题](#其他型月世界常见的图形题)
   1. [应用题](#应用题)
   1. [证明题](#证明题)
      1. [中值定理系](#中值定理系)
         1. [拉日型](#拉日型)
         1. [拉日型例题](#拉日型例题)
      1. [物理应用](#物理应用)
      1. [错题记录](#错题记录)
      1. [我最喜欢的脱裤子放屁题](#我最喜欢的脱裤子放屁题)

## 高中遗留物

+ 毕达哥拉斯三角恒等式
  > $\sin ^{2}\theta +\cos ^{2}\theta =1 \\$
  >> $\tan ^{2}\theta +1\,=\sec ^{2}\theta \\$
  >>> $1\,+\cot ^{2}\theta =\csc ^{2}\theta \\$

+ 幂简约公式
  > $\cos ^{2}\theta=\frac{1}{2}({1+\cos{2\theta}})$
  >> $\sin ^{2}\theta=\frac{1}{2}({1-\cos{2\theta}})$

+ 反函数
  > y=f(x)=w, x=g(y), y=g(x)

+ 变形
  > $a^{-\frac{1}{2}}=\frac{1}{\sqrt{a}}\\e^{\ln{x}}=\ln{e^x}=x\\e^{-\ln{x}}=x^{-1}=\frac{1}{x}$
  >
## 函数

### 一元函数

+ 一次奇
+ 二次偶
+ 三次奇
+ -1次奇
+ sqrt/二分之一次无奇偶性
+ 幂函数无
+ 对数函数无

### 三角函数

+ sin奇
+ cos偶
+ tan奇
+ cot奇
+ arc sin奇
+ arc cos无
+ arc tan奇
+ arc cot无

### 计算法则

+ 奇+奇=偶
+ 偶±偶=偶
+ 奇x奇=偶
+ 奇x偶=奇
+ 偶x偶=偶

### 定义域

> 取x存在的区间

### 函数单调性

> 若函数求一次导后y`>0, 则函数单调递增, 反之递减

### 函数凹凸性

> 若函数求两次导后y``>0, 为极大值, 则函数凹, 反之凸

### 判断函数间断点

> 若 x=n 直接带入原式无定义, 则使用极限推导-> limx=n 原式

## 极限

### 极限类型

七种未定式

+ $\frac{0}{0}$
+ $\frac{\infin}{\infin}$
+ $0\times\infin$
  > $\rightarrow\frac{\infin}{\frac{1}{0}}\rightarrow\frac{\infin}{\infin}$
+ $\infin-\infin$
  > 通分, 和差化积
+ $\infin^0$
  > $e^{0\times\ln\infin}\rightarrow{e^{0\times\infin}}$
+ $0^0$
  > $e^{0\times\ln0}\rightarrow{e^{0\times\infin}}$
+ $1^\infin$
  > $e^{\infin\ln{1}}\rightarrow e^{\infin\times0}$
  >> $x^{f(x)} = 1^∞, 则=e^{f(x)\ln{x}}, 同时 e^{f(x)\ln{x}} = 1^∞$
  >>
### 极限做题时的性质

+ $要使函数f(x)在x=1处连续, 则有\lim_{x\to{1}}f(x)=f(1)$
+ 极限趋近于无穷则没有极限aka.无穷不是极限wtf

### 极限计算法则

+ 确定未定式类型

> Limit properties − if the limit of f(x), and g(x) exists, then:

+ $\lim_{x\to a}$ (x) = a
+ $\lim_{x\to a}$ [c·f(x)] = c·$\lim_{x\to a}$ f(x)
+ $\lim_{x\to a}$ [(f(x))^c] = $\lim_{x\to a}$ f(x)^c
+ $\lim_{x\to a}$ [f(x) ± g(x)] = $\lim_{x\to a}$ f(x) ± $\lim _{x\to a}$ g(x)
+ $\lim_{x\to a}$ [f(x) · g(x)] = $\lim_{x\to a}$ f(x) · $\lim _{x\to a}$ g(x)
+ $\lim_{x\to a}$ [f(x) / g(x)] = $\lim_{x\to a}$ f(x) / $\lim _{x\to a}$ g(x) , where $\lim_{x\to a}$g(x)≠0

### 近似值

+ $\lim_{x\to 0}$ 时
  > $1-x=1 \\ \sin x = x \\ \tan x = x \\ e^x = 1+x \\ e^{x^a} = 1+x^a \\ \ln(1+x) = x \\ 1-\cos{x} = \frac{1}{2x^2} = -(\cos{x} -1)$

#### 利用无穷小的性质求函数的极限

> + 性质1: 有界函数与无穷小的乘积是无穷小
> + 性质2: 常数与无穷小的乘积是无穷小
> + 性质3: 有限个无穷小相加、相减及相乘仍旧无穷小

### 重要极限

+ $\lim_{x\to{0}}\frac{\sin{x}}{x}=1$
+ $\lim_{x\to0}(1-x)^{\frac{a}{-x}}=e^a$
+ $\lim_{x\to{\infin}}(1+\frac{1}{x})^{ax}=e^a$
+ $\lim_{x\to\infin}(1+\frac{1}{ax})^x=e^\frac{1}{a}$
+ limx->0 1-cosx = (x^2)/2
+ limx->0 sqrt1-x = -x/2
+ limx->0 sinx - tanx = -(x^3)/2
+ limx->0 xsin(n/x) = 0, sin为有界函数, x为无穷小量, 无穷小量×有界函数=0
+ limx->1 lnx = x-1

### 无穷大小量

> 无穷大小量的阶数: 趋近于无穷大小时, 将原式作为极限计算, 例如:

+ $\lim_{x\to 0}\left({x^2}\right) = {x^2}$
+ $\lim_{x\to 0}\left({1-\cos{x}}\right) = \frac 1{x^2}$
+ $\lim_{x\to 0}\left(\sqrt{1-{x}}-1\right) = -\frac 1{2}{x}$
+ $\lim_{x\to 0}\left(\sin{x}-\tan{x}\right) = {x}-\frac 1{2}{x^3}$

#### 等价无穷大小

> limx->0 [f(x)/g(x)] = 1, 即为等价无穷小

### 常用解法

+ sinx -> x | sinAx -> Ax

#### 抓大头

> 看最高次幂->前面的系数

### 常见的等价变换

> x->0时

+ sin(x) -> x
+ sin^2(x) -> x^2
+ ln(1+2x) -> 2x
+ xsinx -> x^2

### 泰勒展开

$
\begin{aligned}
e^{x} &=\sum_{n=0}^{\infin} \frac{1}{n !} x^{n}=1+x+\frac{1}{2 !} x^{2}+\cdots \in(-\infin,+\infin) \\
\sin x &=\sum_{n=0}^{\infin} \frac{(-1)^{n}}{(2 n+1) !} x^{2 n+1}=x-\frac{1}{3 !} x^{3}+\frac{1}{5 !} x^{5}+\cdots, x \in(-\infin,+\infin) \\
\cos x &=\sum_{n=0}^{\infin} \frac{(-1)^{n}}{(2 n) !} x^{2 n}=1-\frac{1}{2 !} x^{2}+\frac{1}{4 !} x^{4}+\cdots, x \in(-\infin,+\infin) \\
\ln(1+x) &=\sum_{n=0}^{\infin} \frac{(-1)^{n}}{n+1} x^{n+1}=x-\frac{1}{2} x^{2}+\frac{1}{3} x^{3}+\cdots, x \in(-1,1] \\
\frac{1}{1-x} &=\sum_{n=0}^{\infin} x^{n}=1+x+x^{2}+x^{3}+\cdots, x \in(-1,1) \\
\frac{1}{1+x} &=\sum_{n=0}^{\infin}(-1)^{n} x^{n}=1-x+x^{2}-x^{3}+\cdots, x \in(-1,1)\\
(1+x)^{\alpha} &=1+\sum_{n=1}^{\infin} \frac{\alpha(\alpha-1) \cdots(\alpha-n+1)}{n !} x^{n}=1+\alpha x+\frac{\alpha(\alpha-1)}{2 !} x^{2}+\cdots, x \in (-1,1)\\
\arcsin x &=\sum_{n=0}^{\infin} \frac{(2 n) !}{4^{n}(n !)^{2}(2 n+1)} x^{2 n+1}=x+\frac{1}{6} x^{3}+\frac{3}{40} x^{5}+\frac{5}{112} x^{7}+\frac{35}{1152} x^{9}+\cdots,x \in (-1,1)\\
\arctan x &=\sum_{n=0}^{\infin} \frac{(-1)^{n}}{2 n+1} x^{2 n+1}=x-\frac{1}{3} x^{3}+\frac{1}{5} x^{5}+\cdots+x \in[-1,1]\\
\tan x &= \sum_{n=1}^{\infin} \frac{B_{2 n}(-4)^{n}\left(1-4^{n}\right)}{(2 n) !} x^{2 n-1}=x+\frac{1}{3} x^{3}+\frac{2}{15} x^{5}+\frac{17}{315} x^{7}+\frac{62}{2835} x^{9}+\frac{1382}{155925} x^{11}+\frac{21844}{6081075} x^{13}+\frac{929569}{638512875} x^{15}+\cdots, x \in\left(-\frac{\pi}{2}, \frac{\pi}{2}\right)
\end{aligned}
$

#### 其他型月世界的泰勒展开

+ $\sqrt{1+x} = {(1+x)}^\frac{1}{2} = 1+\frac{1}{2x}+o{x}$
+ $\sqrt{1-x} = {(1-x)}^\frac{1}{2} = 1-\frac{1}{2x}+o{x}$

### 极限的图形学

+ 若有一点使极限值 = 函数值, 则函数在该点连续

### 其他型月世界的极限题

+ $\lim_{n\to\infin}(\frac{1}{\sqrt{1+n^2}}+...+\frac{1}{\sqrt{n+n^2}})=?$
  > 夹逼缩放法:
  >> $\lim_{n\to\infin}\frac{n}{\sqrt{n+n^2}}≤...≤\lim_n{\to\infin}\frac{1}{\sqrt{1+n^2}}$
  >>> 求极限得 1≤...≤1, 即 ...=?=1
+ 极限 limx->0 {[e^(x^2)]-1}/cosx -1
  > 换元法 x^2 = u, 等价 (e^x)-1->x, 变形 cosx-1 = -(1-cosx), limx->0 1-cosx = (x^2)/2, -(1-cosx)=-(x^2)/2, 带入原极限求得 -2
+ $极限 lim_{x=1} x^\frac{1}{x-1}, 原极限为1^∞型, 转换为 e^{\frac{1}{x-1}lnx}\rightarrow lim_{x\to 1}\ln(x), x-1\\带入得 e^{\frac{1}{x-1}x-1}=e, x=1无定义且极限存在, 则x=1为可去间断点$
+ 第一判定定理
  > 啥我也不知道
  >
+ 极值点产生驻点和不可导点, 即函数一阶导=0或不存在.

---

## 导数

定义:
  > $\lim_{x\to 0} \frac{f(X)-f(x)}{X-x}$

### 导数运算法则

+ $(A+-B)' = A' +- B'$
+ $(AB)' = A'B + AB' 前导后不导+后导前不导$
+ $(\frac{A}{B})' = \frac{A'B - AB'}{B^2}$
+ $dz=\frac{\partial{z}}{\partial{x}}dx+\frac{\partial{z}}{\partial{y}}dy$ (这个是全微分)
  > $z`=\frac{\partial{z}}{\partial{x或者y}}$
+ $xy=t,\frac{d^2x}{dy^2}=\frac{dy}{dx}\frac{dt}{dx}$

### 导数公式

$
C`=0, C为常数 \\
x^a`=ax^{a-1} \\
\sqrt{x}`= \frac{1}{2\sqrt{x}} \\
\frac{1}{\sqrt{x}}`=-\frac{1}{2x^{\frac{3}{2}}} \\
a^x`=a^x\ln{a} \\
e^x`=e^x \\
\log_a{x}`=\frac{1}{x}\ln{a} \\
\ln{x}`=\frac{1}{x} \\
\sin{x}`=\cos{x} \\
\cos{x}`=-\sin{x} \\
$

+ (tan x)' = sec^2 x
+ (sec x)' = sec x + tan x
+ (cot x)' = -csc^2 x
+ (csc x)' = -csc x * cot x
+ $\arcsin{x}`=\frac{1}{\sqrt1-x^2} = -\arccos{x}`$
+ $\arctan{x}`=\frac{1}{1+x^2} = -\arccot{x}`$
+ (1/x)' = -1/x^2
+ $\frac{1}{x^2}`=-\frac{2}{x^3}$
+ ln(1+e^x)' = e^x/(1+e^x)
+ ln[f(x)+C]' = 1/[f(x)+C] * f'(x)
+ [e^f(x)^n]' = e^f(x)^n * [f(x)^n]'
+ $(e^{\digamma{(x)}})`=[\digamma{(x)}]`e^{\digamma{(x)}}$
+ $|f(x)|` = \frac {f(x)}{|f(x)|}$
+ $(x^{\sin{x}})`=(e^{\sin{x}\ln{x}})`=(\cos{x}\ln{x}+\frac{\sin{x}}{x})x^{\sin{x}}$

### 隐函数求导

+ 把n元隐函数看作(n+1)元函数, 通过多元函数的偏导数的商求得n元隐函数的导数
+ 针对1元隐函数, 把y看作x的函数, 利用链式法则在隐函数等式两边分别对x求导, 再通过移项求得${\frac {dy}{dx}}$的值
+ 针对2元隐函数, 把y,z看作x的函数, 利用链式法则在隐函数等式两边分别对x求导, 令dz=0, 再通过移项求得${\frac{\partial y}{\partial x}}$的值

#### 隐函数求导例题

+ $设函数y=y(x)由方程\ln(x^2+y)=x^3y+\sin x确定, 求\frac{dy}{dx}|x=0$
  > $复合函数\ln(w)求导得|\frac{w`}{w}|, 复合函数x^3y求导得3x^2y+x^3y`\\\rightarrow\frac{dy}{dx}=\frac{2x+y`}{x^2+y}=3x^2y+x^3y`+\cos x\\带入x=0得\frac{y`}{y}=1,x=0带入原方程得y=1,则\frac{y`}{1}=1, y`=1, 即\frac{dy}{dx}|{x=0} =1$

### 可导导致的连续

$已知函数Q(x)在点x=0处可导,函数f(x)=W(x)Q(W(x)), 则f`(w)=\frac{f(x)-f(w)}{x-w}$

### 分离变量

> 把原式化为xdx=ydy

---

## 偏导数

---

## 积分

逆求导=积分

### 分部积分法

假设${\displaystyle h(x)\ }$与${\displaystyle k(x)\ }$是两个连续可导函数. 由乘积法则可知
> ${\displaystyle {\frac {{\rm {d}}(hk)}{{\rm {d}}x}}={\frac {{\rm {d}}h}{{\rm {d}}x}}k+h{\frac {{\rm {d}}k}{{\rm {d}}x}}}$

不定积分形式的分部积分方程
> ${\displaystyle \int {\frac {{\rm {d}}h}{{\rm {d}}x}}k\ {\rm {d}}x=hk-\int h{\frac {{\rm {d}}k}{{\rm {d}}x}}\ {\rm {d}}x}$

简化
> $\int u\,dv=uv-\int v\,du$

注意:

+ 分布积分u的选择遵守优先级原则:
  > 反三角函数>对数函数>幂函数>指数函数>三角函数>常数>变量

### 变换积分次序

> 画坐标画积分区间, 然后先交写上线balabala从x型换成y型

### 不定积分

计算最后加常数

### 不定积分公式

$
\int x^\mu \mathrm{dx}=\frac1{\mu+1}x^{\mu+1}+C(\mu \ne -1) \\
\int\frac{1}{x}\mathrm{dx}=\ln|x| + C \\
\int a^x \mathrm{dx}=\frac{a^x}{\ln a} + C \\
\int e^{±x} \mathrm{dx}=±e^{±x} + C \\
\int \ln x\mathrm{dx}=x\ln x-x + C \\
\int \sin x \mathrm{dx}=-\cos x + C \\
\int \sin ^2x\mathrm{dx}= \frac{1}{2}\left(x-\frac{1}{2}\sin \left(2x\right)\right)+C\\
\int \cos x \mathrm{dx}=\sin x + C \\
\int \cos^2 x \mathrm{dx}=\frac 1{2}(x+\frac 1{2}\sin{2x}) + C \\
\int\sec x\tan x\mathrm{dx}=\sec x + C \\
\int \sec^2 x \mathrm{dx}=\int \frac1{\cos^2 x}\mathrm{dx}=\tan x + C \\
\int \csc x\cot x\mathrm{dx}=-\csc x + C \\
\int \csc^2x\mathrm{dx}=\int \frac1{\sin^2x}\mathrm{dx}=-\cot x + C \\
\int \tan x\mathrm{dx}=-\ln|\cos x| + C \\
\int \cot x \mathrm{dx}=\ln|\sin x| + C \\
\int \sec x \mathrm{dx}=\ln|\sec x+\tan x| + C \\
\int \csc x\mathrm{dx}=\ln|\csc x -\cot x| + C \\
$

### 经常考的傻逼积分变形

$
\int\frac{1}{x^2}\mathrm{dx}=-\frac{1}{x} + C \\
\int\frac{1}{\sqrt{x}}\mathrm{dx}=2\sqrt{x} + C \\
\int \frac{1}{x+1\:}\mathrm{dx}=\ln|x+1|+C \\
\int \frac{\mathrm{dx}}{x^2+a^2}=\frac1{a}\arctan {\frac{x}{a}} + C \\
\int \frac{\mathrm{dx}}{x^2-a^2}=\frac1{2a}\ln|\frac{x-a}{x+a}| + C \\
\int \frac{\mathrm{dx}}{\sqrt{a^2-x^2}}=\arcsin \frac{x}{a} + C \\
\int \frac{\mathrm{dx}}{\sqrt{x^2\pm a^2}}=\ln|x+\sqrt{x^2\pm a^2}| + C \\
\int \sqrt{a^2-x^2}\mathrm{dx}=\frac{x}{2}\sqrt{a^2-x^2}+\frac{a^2}{2}\arcsin \frac{x}{a} + C \\
\int \sqrt{x^2\pm a^2}\mathrm{dx}=\frac{x}{2}\sqrt{x^2\pm a^2}\pm \frac{a^2}{2}\ln|x+\sqrt{x^2\pm a^2}| + C \\
\int \frac{e^{a\sqrt{x}}}{\sqrt{x}}\mathrm{dx} = \frac{2}{a}{e^{a\sqrt{x}}} + C \\
\int \frac{a}{b-{c{x}}}{dx} = \frac{-a}{c}\ln|{b}-{c}{x}| + C \\
\int \frac{\mathrm{dx}}{\sqrt{1-x^2}}=\arcsin x + C \\
\int \frac{\mathrm{dx}}{1+x^2}=\arctan x + C \\
\int \sin ^2\left(\frac{x}{2}\right)dx=\frac{1}{2}\left(x-\sin \left(x\right)\right)+C\\
\int \cos ^2\left(\frac{x}{2}\right)\mathrm{dx} = \frac 1{2}(x+\sin{x}) + C \\$

### 其他型月世界常见积分

+ $曲线积分\int_Lf(x)ds, ds=\sqrt{1+f`(x)^2}dx$
  > 先找积分区间L
  >> $再找出f(x)的斜率 k=\frac{y_1-y_2}{x_1-x_2}$
  >>> $然后套公式y-y_0=k(x-x_0)求出y=?,x=?,\\ds=什么什么dx$
  >>>> $x,y替换到原函数里得到一个新的积分\\\int_{x的区间}x或者f(x)社么什么dx, 然后求积分就完了$
  >>>>
### 定积分

> 使用换元法积分区间也要变

### 定积分例题

+ $\int_0^1\:(2x+k)dx = 2, k=1$
+ $\int_0^1\:\frac{1}{x+1}$
  > $令x+1=u, 原函数=\int_{0+1}^{1+1}\:\frac{1}{u}du=\ln|u|_1^2\:=\ln{2}-\ln{1}=\ln{2}-0=\ln{2}$

### 二重积分

$性质A: \int\int{区间D}dxdy=区间D图形的面积$

### 变上限积分求导

> 积分上限为参数, 下限为常数, 则 d/dx∫f(t)dt = F(x)-F(a)

### 对称限积分

> $形如\int_{-a}^a\:f(x)dx, 其中$
>> $被积函数为偶函数, 则积分可以写作2\int_0^a\:f(x)dx$
>> $被积函数为奇函数, 则积分等于0$

### 圆相关的积分

> $x^2+y^2=r^2是圆心在圆点的表达式,\int\int_Df(x,y)dxdy的积分可以写成\int\int_Dg(r)dO=\int_\theta d\theta\int_rg(r)rdr, \theta\in[0,2\pi],O是极坐标的什么什么概念衍生, 极坐标是用角度和长度描述位置的坐标系$

---

## 微分

> 微分 d(f(x)) = f'(x)dx

### 微分运算法则

+ d(A+-B) = dA +- dB
+ d(A*B) = A*dB + B*dA
+ d(A/B) = (B*dA - A*dB)/B^2
+ d(C*A) = C*dA
+ d[F(g(x))] = F'(g(x)) * g'(x)

### 微分方程

以下是常微分方程的一些例子, 其中$u$为未知的函数, 自变量为$x$, $c$及$\omega$ 均为常数.

+ 非齐次一阶常系数线性微分方程:
  > ${\displaystyle {\frac {du}{dx}}=cu+x^{2}.}$
+ 齐次二阶线性微分方程:
  > ${\displaystyle {\frac {d^{2}u}{dx^{2}}}-x{\frac {du}{dx}}+u=0.} $
+ 描述谐振子的齐次二阶常系数线性微分方程:
  > ${\displaystyle {\frac {d^{2}u}{dx^{2}}}+\omega ^{2}u=0.} $
+ 非齐次一阶非线性微分方程:
  > ${\displaystyle {\frac {du}{dx}}=u^{2}+1.} $
+ 描述长度为$L$的单摆的二阶非线性微分方程:
  > ${\displaystyle L{\frac {d^{2}u}{dx^{2}}}+g\sin u=0.} $
+ $以下是偏微分方程的一些例子, 其中u为未知的函数, 自变量为x$
  > $x及t或者是x及y$
+ 齐次一阶线性偏微分方程:
  > ${\displaystyle {\frac {\partial u}{\partial t}}+t{\frac {\partial u}{\partial x}}=0.}$
+ 拉普拉斯方程, 是椭圆型的齐次二阶常系数线性偏微分方程:
  > $ ∂ 2 u ∂ x 2 + ∂ 2 u ∂ y 2 = 0. {\displaystyle {\frac {\partial ^{2}u}{\partial x^{2}}}+{\frac {\partial ^{2}u}{\partial y^{2}}}=0.}$
+ KdV方程, 是三阶的非线性偏微分方程:
  > $∂ u ∂ t = 6 u ∂ u ∂ x − ∂ 3 u ∂ x 3 . {\displaystyle {\frac {\partial u}{\partial t}}=6u{\frac {\partial u}{\partial x}}-{\frac {\partial ^{3}u}{\partial x^{3}}}.}$

### 简易微分方程的求解方法

#### 一阶线性微分

+ $形如y`+q(x)y=p(x)的方程$
  > $通解公式: e^{-\int p(x)dx}(\int e^{\int p(x)dx}q(x)dx+C)$
  >
#### 一阶线性常微分方程

> 对于一阶线性常微分方程, 常用的方法是常数变易法:

对于方程:  ${\displaystyle y'+p(x)y+q(x)=0}$

可知其通解: ${\displaystyle y=C(x)e^{-\int p(x)\,dx}}$

然后将这个通解代回到原式中, 即可求出${\displaystylxe C(x)}$的值

#### 二阶常系数齐次常微分方程x

> 对于二阶常系数齐次常微分方程, 常用方法是求出其特征方程的解

对于方程: ${\displaystyle y''+py'+qy=f(x)}$

其特征方程: ${\displaystyle r^{2}+pr+q=0}$

根据其特征方程, 判断根的分布情况, 然后得到方程的齐解: ${\displaystyle y_{h}=c_{1}y_{1}+c_{2}y_{2}}$

> 一般的齐解形式为

(在${\displaystyle r_{1}=r_{2}}$的情况下): ${\displaystyle y_{h}=(C_{1}+C_{2}x)e^{rx}}$

(在${\displaystyle r_{1}\neq r_{2}}$的情况下): ${\displaystyle y_{h}=C_{1}e^{r_{1}x}+C_{2}e^{r_{2}x}}$

(在共轭复数根的情况下): ${\displaystyle y_{h}=e^{\alpha x}(C_{1}\cos(\beta x)+C_{2}\sin(\beta x))}$

接者再解特解yp,可用微分算子计算出 最后,得y=yh+yp

### 其他型月世界得求微分通解

+ $求方程2(xy+y)y`=y的通解$
  > $\begin{aligned}y`=\frac{dy}{dx}&=\frac{y}{2(xy+x)}\\ydx&=2(xy+x)dy\\\frac{y+1}{y}dy&=\frac{1}{2x}dx\\\int\frac{y+1}{y}dy&=\int\frac{1}{2x}dx\\\int 1+\frac{1}{y}dy&=\int\frac{1}{2}\frac{1}{x}dx\\y+\ln{|y|}&=\frac{1}{2}\ln{|x|}+C\\2y+2\ln{|y|}&=\ln{|x|}+C\\e^{2y+2\ln{|y|}}&=e^{\ln{|x|}+\ln{C}}\\e^{2y+\ln^2|y|}&=e^{\ln{|x|}+\ln{C}}\\e^{\frac{1}{\ln{C}}(2y+\ln{|y|^2})}&=e^{\ln{|x|}}\\Ce^{2y}e^{\ln|y|^2}&=x\\由图像知|x|恒>0\\则原方程通解为\\Ce^{2y}y^2&=x\end{aligned}$
+ $求方程y`+xy^2=0的通解$
  > $\begin{aligned}y`=\frac{dy}{dx}&=-xy^2\\dy&=-xdxy^2\\\frac{dy}{y^2}&=-xdx\\\int\frac{1}{y^2}dy&=\int-{x}dx\\-\frac{1}{y}&=-\frac{x^2}{2}+\frac{C}{2}\\y&=\frac{2}{x^2+C}\end{aligned}$

### 全微分

+ 公式
  > $dz=\frac{\partial{z}}{\partial{x}}dx+\frac{\partial{z}}{\partial{y}}dy$
  >> 有点则直接带入微分算子求出值, 再带上dx dy => 值dx+值dy
  >>
### 微分在近似计算种的应用

+ $f(x_0+\Delta{x})=f(x_0)+f`(x_0)\Delta{x}$

---

## 级数

### 级数的敛散

+ 条件收敛: 若级数 ∞∑n=1 |Un| ->发散, ∞∑n=1 Un ->收敛, 则称级数为条件收敛级数
+ 绝对收敛: 若级数 ∞∑n=1 |Un| ->收敛, 则称级数为绝对收敛级数
+ 收敛半径:
  > $收敛半径R=\frac{1}{\rho}, \rho=\lim_{n\to\infin}|\frac{A{n+1}}{An}|, \rho\in{(0,+∞)}$
+ 收敛区间:
  > (-R,R)
+ 收敛域:
  > 先求出收敛半径
  >> 讨论收敛区间端点的敛散性, 即讨论x=-R,x=R时的带入到原级数后的级数的敛散性.
+ 交错级数判别敛散性:
  > $具有以下形式的级数{\displaystyle \sum_{n=0}^{\infin }(-1)^{n}a*{n}\!} \\其中所有的a_{n}非负, 被称作交错级数. \\如果当n趋于无穷时, 数列a_{n}的极限存在且等于0, 并且每个a_{n}小于或等于a_{n-1}(即数列a_{n}是单调递减的), 那么级数收敛. \\如果L是级数的和{\displaystyle \sum_{n=0}^{\infin }(-1)^{n}a_{n}=L\!}那么部分和{\displaystyle S_{k}=\sum_{n=0}^{k}(-1)^{n}a_{n}\!}逼近L有截断误差{\displaystyle \left|S_{k}-L\right\vert \leq \left|S_{k}-S_{k-1}\right\vert =a_{k}\!}$
  >> 简化: $\lim_{n\to\infin}An=0, An≥A{n+1}, 即单调递减$

+ $若级数{\displaystyle \sum_{n=1}^{\infin }Un\! 收敛, 则\lim_{n\to\infin}Un=0}$

#### P级数

> $形如{\displaystyle\sum_{n=1}^{\infin}} \frac{1}{n^p}\\其中n>0时, p>1 则级数收敛, p<=1时级数发散$

#### 交错P级数

> $形如{\displaystyle\sum_{n=1}^{\infin}}(-1)^{n-1} \frac{1}{n^p}\\其中, p>1时绝对收敛, 0<p≤1时条件收敛, p≤0时发散$

#### 调和级数

> Harmonic series 是一个发散的无穷级数, 表达式为:

 ${\displaystyle \sum _{n=1}^{\infty }{\frac {1}{n}}=1+{\frac {1}{2}}+{\frac {1}{3}}+{\frac {1}{4}}+\cdots \,\!}$

#### 和函数

+ $一个等比数列的首n项之和, 称为等比数列和 (sum of geometric sequence)或几何级数 (geometric series), 记作S_{n}$

+ 求法
  > 求级数收敛域, 再令S(x)=这个级数=化简化简, 在求导并令n=0,然后带入求和公式
+ 求和公式
  > $等比数列{\displaystyle S_{n}={\frac {a(1-r^{n})}{1-r}}}\\等差级数{\displaystyle S_{n}={\frac {n}{2}}\,(a+a_{n})\\={\frac {n}{2}}[2a+(n-1)d]\\=an+d\cdot {\frac {n(n-1)}{2}}}$

### 其他型月世界的级数例题

+ $设级数∞∑n=1 Bn 为正项级数, ∞∑n=1 (A_n)^2 收敛, 则级数∞∑n=1 (-1)^n |A_n|/sqrt下n^2 + B_n$
  > 绝对收敛.
+ $求幂级数{\displaystyle\sum_{n=0}^{\infin }\frac{\ln(n+1)}{n}x^{n-1}}的收敛域$
  > $收敛半径R=\frac{1}{\rho},\rho=\lim_{n\to\infin}|\frac{A{n+1}}{A{n}}|, A{n}=\frac{\ln{n+1}}{n}$
  >> $\begin{aligned}\rho&=\lim_{n\to\infin}|\frac{\frac{\ln(n+1+1)}{n+1}}{\frac{\ln(n+1)}{n}}|\\&=\lim_{n\to\infin}|\frac{\ln(n+1+1)}{n+1}\frac{n}{\ln(n+1)}|\\&=\lim_{n\to\infin}\frac{\ln(n+2)}{\ln(n+1)}\frac{n}{n+1}\\&=\lim_{n\to\infin}|\frac{n+1}{n+2}|\\&=1\end{aligned}\\R=\frac{1}{\rho}=1, 原级数收敛区间为(-1,1)$
  >>> $当x=-1时, 原级数={\displaystyle\sum_{n=0}^{\infin }\frac{\ln(n+1)}{n}(-1)^{n-1}}为交错级数, 此时判别敛散性\\\lim_{n\to\infin}\frac{\ln(n+1)}{n}=\frac{\infin}{\infin}=洛\frac{f`(x)}{g`(x)}=0, 且单调递减, 即x=-1时收敛\\当x=1时, 原级数={\displaystyle\sum_{n=0}^{\infin}\frac{\ln(n+1)}{n}(1)^{n-1}}={\displaystyle\sum_{n=0}^{\infin}\frac{\ln(n+1)}{n}}>{\displaystyle\sum_{n=0}^{\infin }\frac{1}{n}},即x=1时小发散\rightarrow大发散$
  >>>> 综上, 原级数收敛区域为[-1,1)
+ $求幂级数\displaystyle\sum_{n=1}^{\infin }\frac{x^n}{3^n}的收敛半径$
  > $a_n=\frac{1}{3^n},\frac{a_n+1}{a_n}=\frac{\frac{1}{3^n+1}}{\frac{1}{3^n}}=\frac{3^n+1}{3^n}=\frac{3^n3}{3^n}=3$

---

## 各类理论

### 中值定理/均值定理

> 給定平面上固定兩端點的可微曲線, 則這曲線在這兩端點間至少有一點, 在這點該曲線的切線的斜率等於兩端點連結起來的直線的斜率
>> $假設函數 f 在閉區間 [a,b] 連續且在開區間 (a,b) 可微, 則存在一點 c , a < c < b, 使得 {\displaystyle f'(c)={\frac {f(b)-f(a)}{b-a}}}.$

### 洛必达

$
洛必达法则可以求出特定函数趋近于某数的极限值, \\
两函数f(x),g(x)在以x=c为端点的开区间可微 {\displaystyle c\in {\bar {\mathbb {R} }}}时\\
如果{\displaystyle\lim_{x\to c}{f(x)}=\lim_{x\to c}{g(x)}=0}或{\displaystyle \lim_{x\to c}{|f(x)|}=\lim_{x\to c}{|g(x)|}=\infin }\\其中一者成立, 则称欲求的极限{\displaystyle\lim_{x\to c}{\frac {f(x)}{g(x)}}}为未定式\\此时洛必达法则表明: {\displaystyle\lim_{x\to c}{\frac {f(x)}{g(x)}}=\lim_{x\to c}{\frac{f'(x)}{g'(x)}}}
$

### 条件

可微代表所有维度下导数存在, 一元下两者等价. 一般情况下, 可微是可导的充分条件[可微一定可导, 可导不一定可微], 可导是连续的充分条件[可导一定连续, 连续不一定可导].

> + 可微=>可导=>连续=>极限存在
> + 一阶偏导数连续=>可微=>一阶偏导数存在

### 牛顿-莱布尼兹 公式

应用条件:
  > 要求被积函数在积分区间内连续

一般类型:
  > 找 ∫ 区间 D 上能否存在 x

### 微分中值定理

如果 R 上的函数 f(x) 满足以下条件:(1) 在闭区间 [a,b] 上连续, 在开区间 (a,b) 内可导, 那么:

#### 罗尔定理

> + 在开区间 (a,b) 内至少存在一点 ξ ,
> + 当 f(a)=f(b) 时,
> + 使得 f'(ξ)=0

#### 拉格朗日中值定理

> + 在开区间 (a,b) 内至少存在一点 ξ ,
> + 使得 f'(ξ)=(f(b)-f(a))/(b-a)

#### 柯西定理

> 用参数方程表示的曲线上至少有一点, 它的切线平行于两端点所在的弦.

### 间断点

定义:
  > 如果函数f在点x连续, 则称x是函数f的连续点；如果函数f在点x不连续, 则称x是函数f的间断点

#### 第一类间断点

定义:
  > 给定一个函数 f(x) 如果 x0 是函数 f(x) 的间断点, 并且 f(x) 在 x0 处的左极限和右极限均存在的点称为第一类间断点.

#### 可去间断点

定义:
  > 若 f(x) 在 x0 处得到左、右极限均存在且相等的间断点, 称为可去间断点. 需要注意的是, 可去间断点需满足 f(x) 在 x0 处无定义, 或在 x0 处有定义但不等于函数 f(x) 在 x0 的左右极限.
  >> 例如 $f\left({x}\right)={x}^{\frac{1}{x-1}}$ x=1 是f(x)的可去间断点: 取极限 $\lim_{x\to 1}{x}^{\frac{1}{x-1}}=e^{\frac {x-1}{x-1}}=e$ 得极限存在, 而 x=1 方程无定义, 故为可去间断点.

#### 跳跃间断点

定义:
  > 设函数 f(x) 在 U(x0) 内有定义, x0 是函数 f(x) 的间断点 (使函数不连续的点), 那么如果左极限 f(x-) 与右极限 f(x+) 都存在, 但 f(x-)≠f(x+), 则称 x0 为 f(x) 的跳跃间断点, 它属于第一间断点.

---

## 图形学

### 向量

+ $\overrightarrow{a}=1i+1j+1k$

### 模长

+ $|\overrightarrow{u}|=\sqrt{A^2+B^2+C^2}$

### 方向余弦

+ ${\{\cos\alpha,\cos\beta,\cos\gamma=\frac{i}{模长},\frac{j}{模长},\frac{k}{模长}\}}$

### 方向角

+ $就是方向余弦的值的角度, \cos\alpha=w, \alpha=方向角$

### 法向量

#### 法向量定义

系数

### 方向向量

#### 方向向量定义

直线分母

### 计算与公式

+ 法向量差积
  > $\overrightarrow{S}=\overrightarrow{n_1}\times\overrightarrow{n_2}=\begin{vmatrix} i & i & k \\ n_1系数_1 & n_1系数_2 & n_1系数_3 \\ n_2系数_1 & n_2系数_2 & n_2系数_3 \end{vmatrix}\\=去掉第一行第一列-去掉第二行第二列+去掉第三行第三列=\begin{vmatrix}n_1系数_2&n_1系数_3\\n_2系数_2&n_2系数_3\end{vmatrix}i-类推j+类推k=交叉相乘再相减i-类推j+类推k$
+ 向量垂直
  > 向量点乘=0
+ 向量平行
  > 平行的原向量点分之模长: $\frac{点}{模}$
+ 切线方程
  > 两线相交一点, 求切线方程
  >> 求交点后, 求所求函数切线的斜率, 即一阶导带入交点
  >>> 斜率和交点带入切线方程公式 0=k(X-x) - (Y-y) 求得.
  >>>> $k=\frac{dy}{dx}=\frac{\frac{dy}{dt}}{\frac{dx}{dt}}$
+ 法线方程
  > 0=w(X-x) - (Y-y), 其中wk=-1 (k是切线方程的斜率)
  >> $w=\frac{dy}{dx}=\frac{\frac{dy}{dt}}{\frac{dx}{dt}}$
  >>
+ 点向式方程
  > $$
+ 直线方程
  > 求出方向向量, 找出经过的点, 带入点向式方程

### 其他型月世界常见的图形题

+ 直线balabala与平面平行
  >即平面法向量与直线方向向量垂直
+ 平行于一个向量的单位向量
  > 平行的向量除以目标向量的模长得到通向的单位向量, 此时再加上负号得到反向的单位向量
+ $求曲线x=e^t\cos t,y=e^t\sin t,在t=\frac{\pi}{2}处的法线方程$
  > $设法线的斜率为w, 曲线切线的斜率为k,则w=-k\\k=\frac{dy}{dx}=\frac{\frac{dy}{dt}}{\frac{dx}{dt}}=\frac{e^t\sin t+e^t\cos t}{e^t\cos t-e^t\sin t}=-1, 即w=1\\当t=\frac{\pi}{2}时, x=0, y=e^\frac{\pi}{2}\\此时法线方程为(y-e^\frac{pi}{2})=(x-0), 即y=x+e^\frac{\pi}{2}$

---

## 应用题

+ 圈地求极值型
  > 列出方程, 1次导数得值, 二次导数证明极值
  >
## 证明题

### 中值定理系

#### 拉日型

+ 常见于问两个点使balabala成立
  > 首先作辅助函数 (我也不知道怎么做. 大概和画辅助线一样属于和出题人思路碰一碰)
  > 然后画数轴, 设一点分割另外两点, 分类讨论一下
  >
#### 拉日型例题

+ $已知函数f(x)在[0,1]上连续,在(0,1)内可导,且f(0)=0,f(1)=1, 试证明:\\(1)存在\xi\in(0,1)使f(\xi)=1-\xi\\(2)存在两个不同的点\eta,\mu\in(0,1)使f`(\eta)f`(\mu)=1$
  > $(1)设辅助函数F(\xi)=f(\xi)-1+\xi\rightarrow F(x)=f(x)-1+x, x\in(0,1)$
  >> $F(0)=f(0)-1+0=-1<0, F(1)=f(1)-1+1=1>0, 由零点定理可得必有一点\xi使F(\xi)=0, 即f(\xi)=1-\xi$
  >
  > $(2)设一点\xi使\eta\in(0,\xi),\mu\in(\xi,1)\\∵f(x)在[0,1]上连续,在(0,1)内可导,∴f(x)在[0,\xi]上连续,在(0,\xi)内可导,[\xi,1]上连续,在(\xi,1)内可导\\由拉日定理得f`(\eta)=\frac{f(\xi)-f(0)}{\xi-0},f`(\mu)=\frac{f(1)-\xi}{1-\xi},联立f(\xi)=1-\xi得f`(\eta)f`(\mu)=\frac{1-1+\xi}{\xi}=1,证毕$

### 物理应用

+ 牛顿第二定律
  > 物体质量m, 物体加速度a, 物体动能 F=ma
  >> $加速度a=\frac{d^2x}{dt^2}=\frac{dv}{dt}=v\frac{dv}{dt}, 分离变量可以积分$
  >>> $正比F=kt, 反比Ft=k, 一正一反Fv=kt$
  >>>> $若有积分求得balabala+C, 还要带入条件求出C的具体值, 然后再带入求得的积分成为函数式$
  >>>>
+ 变化率问题
  > $形如t时刻某量y对t的变化率与t时刻某量w成正比 aka. \frac{dy}{dt}=kW(x)$
  
---

### 错题记录

+ $已知f(x)=1-\frac{1}{x}, 则f(f(x))=1-\frac{1}{\frac{1}{1-x}}=1-分子分母同乘x然后化简=\frac{1}{1-x}$ 化简题吃屎去吧
+ $极限\lim_{x\to\infin}\frac{1-2x-x^2}{x^2}=$
  > $\frac{1}{x^2}-\frac{2x}{x^2}-\frac{x^2}{x^2}=\frac{1}{x^2}-\frac{2}{x}-1=带入无穷=0-0-1=-1$ 化简题吃屎去吧
+ $当x\to{0}时2-2\cos{x}~ax^2=$
  > $\lim_{x\to{0}}\frac{2-2\cos{x}}{ax^2}=1,其中1-\cos{x}=\frac{x}{2}, 带入然后解得a=1$
+ $函数f(x)=1-|x-1|在点x=1$
  > $处连续但不可导, ∵左极限≠右极限$
+ $若曲线f(x)=1-x^3与曲线g(x)=\ln{x}在自变量x=x0时的切线相互垂直, 则x0
  > $x0=f`(x0)g(`x0)=-3x^2×\frac{1}{x}=0, x0=\frac{1}{3}$
+ $设函数f(x)在区间(-1,1)内连续, 若x\in(-1,0)时,f`(x)<0;x\in(0,1)时,f`(x)>0,则在区间(-1,1)内$
  >$f(0)是函数的极小值 (极值第一判定)$
+ 向量a=j+k的方向角是啥?
  > $\frac{\pi}{2},\frac{\pi}{4},\frac{\pi}{4}小编也不知道.$
+ $已知e^{-x}是微分方程y``+3ay`+2y=0的一个解, 则a=?.$
  > $令y=e^x,y`=-e^{-x},y``=e^x, 带入原式得e^x-3ae^{-x}+2e^x=0, 即1-3a+2=0, a=1$
+ $\int\int{区间D}f(x,y)dw=\int_0^1\:dx\int_0^x\:f(x,y)dy变换积分次序:$
  > $交换区间, 0≤x≤1, 0≤y≤x可以换为y≤x≤1,0≤y≤1, 则交换后的积分次序为\int_0^1\:dy\int_y^1\:f(x,y)dx$
+ $函数f(x)=e^{1-x}在点x=0.99处的近似值$
  > $magik变换之x=1,\Delta{x}=-0.01, f(0.99)=f(x+\Delta{x})=f(x)+\Delta{x}f`(x)=e^{1-1}+(-0.01)(-e^{1-1})=1+0.01$
  >
### 我最喜欢的脱裤子放屁题

+ 用钢板做成一个表面积为54平方米的有盖长方体水箱, 欲使水箱的容积最大, 则水箱的最大容积为?
+ $设D=\{(x,y)|1≤x^2+y^2≤4,x≥0,y≥0\}则二重积分\int\int_{D}4dxdy=?$
